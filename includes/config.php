<?php

    // Principais Dados do Cliente
    $nome_empresa = "INSTALINOX";
    $emailContato = "comercial@instalinox.com.br";

    // Parâmetros de Unidade
    $unidades = array(
        1 => array(
            "nome" => "INSTALINOX",
            "rua" => "R. Lílian, 67",
            "bairro" => "Parque Barão do Amapá",
            "cidade" => "Duque de Caxias",
            "estado" => "Rio de Janeiro",
            "uf" => "RJ",
            "cep" => "25235-470",
            "latitude_longitude" => "", // Consultar no maps.google.com
            "ddd" => "21",
            "telefone" => "3848-0450",
            "whatsapp" => "96479-1110",
            "link_maps" => "" // Incorporar link do maps.google.com
        ),
        2 => array(
            "nome" => "",
            "rua" => "",
            "bairro" => "",
            "cidade" => "",
            "estado" => "",
            "uf" => "",
            "cep" => "",
            "ddd" => "",
            "telefone" => ""
        )
    );
    
    // Parâmetros para URL
    $padrao = new classPadrao(array(
        // URL local
        "http://localhost/informacoes.instalinox.com.br/",
        // URL online
        "https://www.informacoes.instalinox.com.br/"
    ));
    
    // Variáveis da head.php
    $url = $padrao->url;
    $canonical = $padrao->canonical;
	
    // Parâmetros para Formulário de Contato
    $smtp_contato            = ""; // Solicitar ao líder do dpto técnico, 177.85.98.119
    $email_remetente         = ""; // Criar no painel de hospedagem, admin@...
    $senha_remetente         = "c0B1S3vH5eCvAO";

    // Contato Genérico (para sites que não se hospedam os e-mails)
    // $smtp_contato            = "111.111.111.111";
    // $email_remetente         = "formulario@temporario-clientes.com.br";
    // $senha_remetente         = "4567FGHJK";

    // Recaptcha Google
    $captcha                 = false; // https://www.google.com/recaptcha/
    $captcha_key_client_side = "";
    $captcha_key_server_side = "";

    // CSS default
    $padrao->css_files_default = array(
        "default/reset",
        "default/grid-system",
        "default/main",
        "default/slicknav-menu",
        "_main-style"
    );
    
    // JS Default
    $padrao->js_files_default = array(
        "default/jquery-1.9.1.min",
        "default/modernizr",
        "default/jquery.slicknav.min",
        "jquery.padrao.main"
    );
        
    // Listas de Palavras Chave
    $palavras_chave = array(
        "Coifas Industriais no Rio de Janeiro",
"Coifas Industriais no RJ",
"Coifas Industriais para Cozinha",
"Coifas Industriais para Restaurantes",
"Coifas Industriais para Hamburgueria",
"Coifas Industriais para Pizzaria",
"Coifas Industriais para Escola",
"Coifas Industriais para Forno",
"Coifas Industriais para Churrascaria",
"Coifas de Inox no Rio de Janeiro",
"Coifa de Aço Inox no Rio de Janeiro",
"Instalação de Coifas no Rio de Janeiro",
"Instalação de Exaustores no Rio de Janeiro",
"Bancada de Inox no RJ",
"Bancada de Inox no Rio de Janeiro",
"Bancada de Aço Inox",
"Exaustores no Rio de Janeiro",
"Exaustores Industrias no RJ",
"Exaustores para Cozinha Industrial",
"Exaustores para Hamburgueria",
"Exaustores para Escolas",
"Exaustores para Churrascaria",
"Instalação de Exaustores no Rio de Janeiro",
"Exaustores para Fogão Industrial",
"Bancada de Inox Sob Medida",
"Bancada de apoio em Aço Inox",
"Bancada com cuba em Aço Inox",
"Bancada de Aço Inox no Rio de Janeiro",
"Mesa de Aço Inox no Rio de Janeiro",
"Mesa de Aço Inox com Cuba no Rio de Janeiro",
"Mesa de Aço Inox com Cuba",
"Mesa de Aço Inox com Pia",
"Sistema de Exaustão no Rio de Janeiro",
"Sistema de Exaustão no RJ",
"Sistema de Exaustão para Restaurantes",
"Sistema de Exaustão para Cozinha Industrial",
"Sistema de Exaustão para Churrascaria",
"Sistema de Exaustão para Escolas",
"Sistema de Exaustores no Rio de Janeiro",
"Fabricante de Chapa em Aço Inox",
"Exaustor Centrífugo em Aço Inox"
    );
   
    $palavras_chave_com_descricao = array(
        "Item 1" => "Lorem ipsum dolor sit amet.",
        "Item 2" => "Laudem dissentiunt ut per.",
        "Item 3" => "Solum repudiare dissentiunt at qui.",
        "Item 4" => "His at nobis placerat.",
        "Item 5" => "Ei justo lucilius nominati vim."
    );
    
     /**
     * Submenu
     * 
     * $opcoes = array(
     * "id" => "",
     * "class" => "",
     * "limit" => 9999,
     * "random" => false
     * );
     * 
     * $padrao->subMenu($palavras_chave, $opcoes);
     * 
     */

    /**
     * Breadcrumb
     * 
     * -> Propriedades
     * 
     * Altera a url da Home no breadcrumb
     * $padrao->breadcrumb_url_home = "";
     * 
     * Altera o texto que antecede a Home
     * $padrao->breadcrumb_text_before_home = "";
     * 
     * Altera o texto da Home no breadcrumb
     * $padrao->breadcrumb_text_home = "Home";
     * 
     * Altera o divisor de níveis do breadcrumb
     * $padrao->breadcrumb_spacer = " » ";
     * 
     * -> Função
     * 
     * Cria o breadcrumb
     * $padrao->breadcrumb(array("Informações", $h1));
     * 
     */

    /**
     * Lista Thumbs
     * 
     * $opcoes = array(
     * "id" => "",
     * "class_div" => "col-md-3",
     * "class_section" => "",
     * "class_img" => "img-responsive",
     * "title_tag" => "h2",
     * "folder_img" => "imagens/thumbs/",
     * "extension" => "jpg",
     * "limit" => 9999,
     * "type" => 1,
     * "random" => false,
     * "text" => "",
     * "headline_text" => "Veja Mais"
     * );
     * 
     * $padrao->listaThumbs($palavras_chave, $opcoes);
     * 
     */
    
    /**
     * Funções Extras
     * 
     * $padrao->formatStringToURL();
     * Reescreve um texto em uma URL válida
     * 
     */